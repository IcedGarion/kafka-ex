### Come eseguire
- Install kafka: https://kafka.apache.org/quickstart <br>
Download kafka e unzip come da guida, poi lanciare due applicazioni che si trovano nella cartella scaricata:

- Start zookeper service: bin/zookeeper-server-start.sh config/zookeeper.properties
- Start broker service: bin/kafka-server-start.sh config/server.properties
- Nella cartella del progetto, modificare <b>resources/kafka.properties</b> inserendo <b>bootstrapAddress</b> se diverso da default
- Start <b>KafkaApplicationTests</b>