package com.example.messaging;

import com.example.messaging.bean.PayloadBean;
import com.example.messaging.converter.MessageToPayloadBeanConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
public class KafkaConsumerUser {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumerUser.class);

    private CountDownLatch latch = new CountDownLatch(1);

    // contiene info di contorno al messaggio (topic, event, string payload)
    private ConsumerRecord<String, String> message = null;

    // contiene oggetto deserializzato
    private PayloadBean payload;

    @Autowired
    private MessageToPayloadBeanConverter converter;

    @KafkaListener(topics = KafkaConfig.EMPLOYEE_KAFKA_TOPIC, groupId = "groupId")
    public void receive(ConsumerRecord<String, String> consumerRecord) {
        LOGGER.info("received payload='{}'", consumerRecord.toString());

        setMessage(consumerRecord);

        try
        {
            setPayload(converter.convert(consumerRecord.value()));
        }
        catch (JsonProcessingException e) {
            LOGGER.error("Unable to parse message: '{}'", consumerRecord.value());
        }

        latch.countDown();
    }

    public CountDownLatch getLatch() {
        return latch;
    }

    public ConsumerRecord<String, String> getMessage() {
        return message;
    }

    private void setMessage(ConsumerRecord<String, String> message) {
        this.message = message;
    }

    public PayloadBean getPayload() { return payload; }

    public void setPayload(PayloadBean payload) { this.payload = payload; }

}
