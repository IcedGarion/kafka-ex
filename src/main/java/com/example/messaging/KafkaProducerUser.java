package com.example.messaging;

import com.example.messaging.bean.PayloadBean;
import com.example.messaging.converter.PayloadBeanToMessageConverter;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducerUser{
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducerUser.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private PayloadBeanToMessageConverter converter;

    public void send(String topic, String event, PayloadBean payload) {
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topic);

        try {
            kafkaTemplate.send(topic, event, converter.convert(payload));
        }
        catch (JsonProcessingException e) {
            LOGGER.error("Unable to serialize object: '{}'", payload);
        }
    }
}
