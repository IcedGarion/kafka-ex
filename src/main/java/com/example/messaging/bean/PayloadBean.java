package com.example.messaging.bean;

import java.util.Objects;

public class PayloadBean {
    private String name;
    private String description;

    public PayloadBean(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public PayloadBean() {}

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PayloadBean that = (PayloadBean) o;
        return Objects.equals(name, that.name) && Objects.equals(description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description);
    }

    @Override
    public String toString() {
        return "PayloadBean{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
