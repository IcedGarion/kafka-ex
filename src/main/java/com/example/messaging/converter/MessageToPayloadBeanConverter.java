package com.example.messaging.converter;

import com.example.messaging.bean.PayloadBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class MessageToPayloadBeanConverter {
    public PayloadBean convert(String payloadMessage) throws JsonProcessingException {
        return new ObjectMapper().readValue(payloadMessage, PayloadBean.class);
    }
}
