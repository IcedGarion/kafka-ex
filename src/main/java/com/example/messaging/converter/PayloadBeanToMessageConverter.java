package com.example.messaging.converter;

import com.example.messaging.bean.PayloadBean;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class PayloadBeanToMessageConverter {
    public String convert(PayloadBean payload) throws JsonProcessingException {
        return new ObjectMapper().writeValueAsString(payload);
    }
}
