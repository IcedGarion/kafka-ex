package com.example.messaging;

import com.example.messaging.bean.PayloadBean;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class KafkaApplicationTests {

	@Autowired
	private KafkaProducerUser producer;

	@Autowired
	private KafkaConsumerUser consumer;

	@Test
	void testWorkflow() throws Exception {
		PayloadBean payload = new PayloadBean("testName", "testDescription");

		// send
		producer.send(KafkaConfig.EMPLOYEE_KAFKA_TOPIC, "testEvent", payload);

		// receive
		consumer.getLatch().await(10000, TimeUnit.MILLISECONDS);

		assertEquals(0, consumer.getLatch().getCount());
		assertEquals(KafkaConfig.EMPLOYEE_KAFKA_TOPIC, consumer.getMessage().topic());
		assertEquals("testEvent", consumer.getMessage().key());

		PayloadBean response = consumer.getPayload();
		assertEquals(payload, response);
	}
}
